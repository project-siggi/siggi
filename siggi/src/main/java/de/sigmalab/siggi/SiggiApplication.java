package de.sigmalab.siggi;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import reactor.spring.context.config.EnableReactor;
import de.sigmalab.siggi.gw.Builds;
import de.sigmalab.siggi.gw.InMemoryBuilds;
import de.sigmalab.siggi.gw.InMemoryProjects;
import de.sigmalab.siggi.gw.Project;
import de.sigmalab.siggi.gw.Projects;

/**
 * Entrypoint for Siggi-Gateway.
 *
 * @author jbellmann
 */
@EnableAutoConfiguration
@ComponentScan
@Configuration
@EnableReactor
public class SiggiApplication {

	public static void main(final String[] args) {
		SpringApplicationBuilder appBuilder = new SpringApplicationBuilder(SiggiApplication.class);
		appBuilder.listeners(new SimpleApplicationListener());
		appBuilder.initializers(new SiggiHomeDirectoryApplicationContextInitializer());

		appBuilder.run(args);
	}

	@Bean
	public Projects projects() {
		InMemoryProjects inMemoryProjects = new InMemoryProjects();

		//
		Project p1 = new Project("jbellmann", "project-template", "https://bitbucket.org/htw-avs/project-template.git");
		p1.setActive(true);
		inMemoryProjects.addProject(p1);

		//
		Project p2 = new Project("jbellmann", "junit-ext", "https://github.com/sigmalab-projects/junit-ext.git");
		p2.setActive(false);
		inMemoryProjects.addProject(p2);

		return inMemoryProjects;
	}

	@Bean
	@Profile({ SiggiProfiles.DEVELOPMENT, SiggiProfiles.T_1 })
	public Builds builds() {
		return new InMemoryBuilds();
	}

	@Bean
	@Profile({ SiggiProfiles.PRODUCTION, SiggiProfiles.T_2 })
	public Builds productionBuilds() {
		return new InMemoryBuilds();
	}
}
