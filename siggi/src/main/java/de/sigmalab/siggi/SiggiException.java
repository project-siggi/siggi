package de.sigmalab.siggi;

/**
 * Base-Exception for all Siggi-Exceptions.
 * @author jbellmann
 *
 */
public class SiggiException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public SiggiException(){
		super();
	}
	
	public SiggiException(String message){
		super(message);
	}
	
	public SiggiException(String message, Throwable cause){
		super(message, cause);
	}

}
