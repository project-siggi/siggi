package de.sigmalab.siggi;

import java.io.File;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.util.StringUtils;

/**
 * @author jbellmann
 */
public class SiggiHomeDirectoryApplicationContextInitializer implements
		ApplicationContextInitializer<ConfigurableApplicationContext> {

	private static final String SIGGI_HOME = "siggi.home";

	private static final Logger LOG = LoggerFactory.getLogger(SiggiHomeDirectoryApplicationContextInitializer.class);

	@Override
	public void initialize(final ConfigurableApplicationContext applicationContext) {
		String siggiHomeDirectory = applicationContext.getEnvironment().getProperty(SIGGI_HOME);
		if (StringUtils.hasText(siggiHomeDirectory)) {
			LOG.info("'siggi.home' property found : {}", siggiHomeDirectory);
			validateSiggiHomeDirectoryLayoutExists(applicationContext);
		} else {
			LOG.info("'siggi.home.directory' property not found");
			createSiggiHomeDirectoryLayout(applicationContext);
		}
	}

	protected void createSiggiHomeDirectoryLayout(final ConfigurableApplicationContext applicationContext) {
		String siggiHomeDirectoryPath = applicationContext.getEnvironment().getProperty(SIGGI_HOME);
		File siggiHomeDirectory = new File(siggiHomeDirectoryPath);
		initializeSiggiHomeDirectory(siggiHomeDirectory, applicationContext);
		// if (applicationContext.getEnvironment().acceptsProfiles(SiggiProfiles.DEVELOPMENT)) {
		//
		// } else {
		// // String userDir = System.getProperty("user.home");
		// // File siggiHomeDirectory = new File(new File(userDir), ".siggi");
		// initializeSiggiHomeDirectory(siggiHomeDirectory, applicationContext);
		// }

		validateSiggiHomeDirectoryLayoutExists(applicationContext);
	}

	protected void initializeSiggiHomeDirectory(final File siggiHomeDirectory,
			final ConfigurableApplicationContext applicationContext) {
		if (siggiHomeDirectory.mkdirs()) {
			LOG.info("SIGGI_HOME created at : {}", siggiHomeDirectory.getAbsolutePath());
		} else {
			LOG.warn("SIGGI_HOME not created, maybe exists at : {}", siggiHomeDirectory.getAbsolutePath());
		}

		Properties props = new Properties();
		props.setProperty(SIGGI_HOME, siggiHomeDirectory.getAbsolutePath());
		applicationContext.getEnvironment().getPropertySources()
				.addFirst(new PropertiesPropertySource("siggi.home", props));
	}

	protected void validateSiggiHomeDirectoryLayoutExists(final ConfigurableApplicationContext applicationContext) {
		String siggiHomeDirectoryPath = applicationContext.getEnvironment().getProperty(SIGGI_HOME);
		LOG.info("Validate 'SIGGI_HOME' directory layout at : {}", siggiHomeDirectoryPath);

		File siggiHomeDirectory = new File(siggiHomeDirectoryPath);
		String dbDirectoryPath = createDbDirectory(siggiHomeDirectory);
		String usersDirectoryPath = createUsersDirectory(siggiHomeDirectory);
	}

	protected String createDbDirectory(final File siggiHomeDirectory) {

		return createDirectory(siggiHomeDirectory, "db");
	}

	protected String createUsersDirectory(final File siggiHomeDirectory) {

		return createDirectory(siggiHomeDirectory, "users");
	}

	protected String createDirectory(final File siggiHomeDirectory, final String directoryName) {
		File directory = new File(siggiHomeDirectory, directoryName);
		if (!directory.mkdirs()) {
			LOG.info("{}-directory not created, maybe exists at : {}", directoryName, directory.getAbsolutePath());
		} else {
			LOG.info("{}-directory created at : {}", directoryName, directory.getAbsolutePath());
		}

		return directory.getAbsolutePath();
	}

}
