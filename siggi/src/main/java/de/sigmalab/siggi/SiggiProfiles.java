package de.sigmalab.siggi;

/**
 * @author jbellmann
 */
public abstract class SiggiProfiles {
	public static final String DEVELOPMENT = "development";
	public static final String PRODUCTION = "production";
	public static final String T_1 = "0100";
	public static final String T_2 = "0200";
}
