package de.sigmalab.siggi;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * To run the app in tomcat.
 * 
 * @author jbellmann
 */
public class SiggiServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SiggiApplication.class).listeners(new SimpleApplicationListener())
				.initializers(new SiggiHomeDirectoryApplicationContextInitializer());
	}

}
