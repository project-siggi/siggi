package de.sigmalab.siggi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.config.ConfigFileApplicationListener;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;

import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;

/**
 * Listens when the {@link Environment} is prepared.
 *
 * @author  jbellmann
 */
public class SimpleApplicationListener implements SmartApplicationListener {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleApplicationListener.class);

    @Override
    public void onApplicationEvent(final ApplicationEvent event) {
        ApplicationEnvironmentPreparedEvent environmentPreparedEvent = (ApplicationEnvironmentPreparedEvent) event;
        LOG.warn("ApplicationEnvironmentPrepared event raised");
        environmentPreparedEvent.getEnvironment().setActiveProfiles(SiggiProfiles.DEVELOPMENT);
        LOG.info("SET ACTIVE PROFILE TO : {}",
            environmentPreparedEvent.getEnvironment().getActiveProfiles().toString());
    }

    /**
     * Make sure you have an value lower than {@link ConfigFileApplicationListener#DEFAULT_ORDER}.<br/>
     * {@link Ordered#HIGHEST_PRECEDENCE} + 9 should also be possible.
     */
    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 5;
    }

    @Override
    public boolean supportsEventType(final Class<? extends ApplicationEvent> eventType) {

        return ApplicationEnvironmentPreparedEvent.class.isAssignableFrom(eventType);
    }

    @Override
    public boolean supportsSourceType(final Class<?> sourceType) {

        return SpringApplication.class.isAssignableFrom(sourceType);
    }

}
