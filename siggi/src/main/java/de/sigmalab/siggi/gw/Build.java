package de.sigmalab.siggi.gw;

public class Build {

    private long buildnumber;

    private String id;

    private String projectId;

    private String state = "ENQUEUED";

    // for serializers
    protected Build() { }

    public Build(final String projectId, final long buildnumber) {
        this.buildnumber = buildnumber;
        this.projectId = projectId;
        this.id = createBuildId(this.projectId, this.buildnumber);
    }

    public String getId() {
        return id;
    }

    public long getBuildnumber() {
        return buildnumber;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    static String createBuildId(final String projectId, final long buildnumber) {
        StringBuilder sb = new StringBuilder();
        sb.append(projectId).append("/").append(String.valueOf(buildnumber));
        return sb.toString();
    }

}
