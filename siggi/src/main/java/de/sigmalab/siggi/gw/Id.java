package de.sigmalab.siggi.gw;

import com.google.common.base.Joiner;

public class Id {

    private static final Joiner joiner = Joiner.on("/").skipNulls();

    private static String joinIdParts(final String... strings) {
        return joiner.join(strings);
    }

    public static String buildProjectId(final String account, final String project) {
        return joinIdParts(account, project);
    }

    public static String buildBuildId(final String account, final String project, final String buildnumber) {
        return joinIdParts(account, project, buildnumber);
    }

}
