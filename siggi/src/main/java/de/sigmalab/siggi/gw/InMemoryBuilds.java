package de.sigmalab.siggi.gw;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 * @author jbellmann
 *
 */
public class InMemoryBuilds implements Builds {
	
	private final Map<String,AtomicInteger> counters = new HashMap<String,AtomicInteger>(0);

	@Override
	public synchronized int nextBuildnumber(String projectId) {
		AtomicInteger projectCounter = this.counters.get(projectId);
		if(projectCounter == null){
			projectCounter = new AtomicInteger(0);
			this.counters.put(projectId, projectCounter);
		}
		return projectCounter.incrementAndGet();
	}

}
