package de.sigmalab.siggi.gw;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import de.sigmalab.siggi.SiggiException;

/**
 * @author  jbellmann
 */
public class InMemoryProjects implements Projects {

    private Map<String, Project> projects = new HashMap<String, Project>(0);
    private Map<String, Build> builds = new HashMap<String, Build>();

    @Override
    public Iterable<Project> findAllProjectsWithUrl(final String url) {
        List<Project> result = new LinkedList<Project>();
        for (Project p : projects.values()) {
            if (url.equals(p.getUrl())) {
                result.add(p);
            }
        }

        return result;
    }

    @Override
    public void addProject(final Project project) {
        Project p = this.projects.get(project.getProjectId());
        if (p != null) {
            throw new SiggiException("Project allready exist");
        } else {
            this.projects.put(project.getProjectId(), project);
        }
    }

    @Override
    public void removeProject(final String projectId) {
        Project p = this.projects.remove(projectId);
        if (p == null) {
            throw new SiggiException("Project not found:");
        }
    }

    public void addBuild(final Build build) {
        Build b = this.builds.get(build.getId());
        if (b != null) {
            throw new SiggiException("Build allready exist");
        } else {
            this.builds.put(build.getId(), build);
        }
    }

    public Build getBuild(final String id) {
        return this.builds.get(id);
    }

    public void save(final Build build) {
        this.builds.put(build.getId(), build);
    }

    @Override
    public List<Project> findProjectsForAccount(final String account) {
        final String prefix = account + "/";
        List<Project> result = Lists.newArrayList();
        for (Map.Entry<String, Project> entry : this.projects.entrySet()) {
            if (entry.getKey().startsWith(prefix)) {
                result.add(entry.getValue());
            }
        }

        return result;
    }

    @Override
    public List<Build> findBuildsForProject(final String buildProjectId) {
        List<Build> builds = Lists.newArrayList();
        for (Map.Entry<String, Build> entry : this.builds.entrySet()) {
            if (entry.getKey().startsWith(buildProjectId)) {
                builds.add(entry.getValue());
            }
        }

        return builds;
    }

    @Override
    public List<Build> findRunningBuildsForAccount(final String account) {

        Predicate<Map.Entry<String, Build>> predicates = Predicates.and(Predicates.notNull(),
                new BuildStatusPredicate("RUNNING"));

        Iterable<Build> results = filter(this.builds.entrySet(), predicates);
        return Lists.newArrayList(results);
    }

    @Override
    public List<Build> findFinishedBuildsForAccount(final String account) {
        Predicate<Map.Entry<String, Build>> predicates = Predicates.and(Predicates.notNull(),
                Predicates.or(new BuildStatusPredicate("SUCCESS"), new BuildStatusPredicate("FAILED")));

        Iterable<Build> results = filter(this.builds.entrySet(), predicates);
        return Lists.newArrayList(results);
    }

    protected Iterable<Build> filter(final Iterable<Map.Entry<String, Build>> source,
            final Predicate<Map.Entry<String, Build>> predicate) {
        return Iterables.transform(Iterables.filter(source, predicate),
                new Function<Map.Entry<String, Build>, Build>() {

                    @Override
                    public Build apply(final Map.Entry<String, Build> input) {
                        return input.getValue();
                    }
                });
    }

    static class BuildStatusPredicate implements Predicate<Map.Entry<String, Build>> {

        private String state;

        public BuildStatusPredicate(final String state) {
            this.state = state;
        }

        @Override
        public boolean apply(final Entry<String, Build> input) {
            if (input.getValue() == null) {
                return false;
            }

            return state.equals(input.getValue().getState());
        }

    }

    static class StartsWithPredicate implements Predicate<Map.Entry<String, Build>> {

        private final String prefix;

        public StartsWithPredicate(final String prefix) {
            this.prefix = prefix;
        }

        @Override
        public boolean apply(final Entry<String, Build> input) {
            return input.getKey().startsWith(prefix);
        }

    }

}
