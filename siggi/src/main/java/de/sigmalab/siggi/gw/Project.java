package de.sigmalab.siggi.gw;

import com.google.common.base.MoreObjects;

public class Project {

	private String projectId;

	private String account;

	private String name;

	private String url;

	private boolean active = false;

	protected Project() {
		//
	}

	public Project(String account, String name) {
		this(account, name, null);
	}

	public Project(String account, String name, String url) {
		this.projectId = account + "/" + name;
		this.account = account;
		this.name = name;
		this.url = url;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getProjectId() {
		return projectId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String toString() {
		return MoreObjects.toStringHelper(this).add("account", account).add("name", name).add("url", url)
				.add("id", projectId).add("active", active).toString().toString();
	}

}
