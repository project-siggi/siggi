package de.sigmalab.siggi.gw;

import java.util.List;

public interface Projects {

    Iterable<Project> findAllProjectsWithUrl(String url);

    void addProject(Project project);

    void removeProject(String projectId);

    void addBuild(final Build build);

    Build getBuild(final String id);

    void save(final Build build);

    List<Project> findProjectsForAccount(String account);

    List<Build> findBuildsForProject(String buildProjectId);

    List<Build> findRunningBuildsForAccount(String account);

    List<Build> findFinishedBuildsForAccount(String account);

}
