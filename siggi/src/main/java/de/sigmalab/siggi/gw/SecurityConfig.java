package de.sigmalab.siggi.gw;

/**
 * DISABLED FOR NOW
 * 
 * @author jbellmann
 */
// @Configuration
// @EnableWebSecurity
// @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig { // extends WebSecurityConfigurerAdapter {

	// private static final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);
	//
	// @Autowired
	// private Environment environment;
	//
	// @Override
	// @Autowired
	// protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
	// logger.info("Active-Profiles : {}", environment.getActiveProfiles().toString());
	// if (environment.acceptsProfiles("live")) { }
	// else if (environment.acceptsProfiles("integration", "local", "default", "development")) {
	// auth.inMemoryAuthentication().withUser("klaus").password("klaus").roles("USER");
	// }
	// }
	//
	// @Configuration
	// @Order(1)
	// public static class ApiSecurityConfig extends WebSecurityConfigurerAdapter {
	//
	// @Override
	// public void configure(final WebSecurity web) throws Exception {
	//
	// // debug, make it configurable
	// web.debug(false);
	// }
	//
	// @Override
	// protected void configure(final HttpSecurity http) throws Exception {
	//
	// // http.antMatcher("/api/**").authorizeRequests().antMatchers("/api/**").hasRole("USER").and().httpBasic();
	// http.antMatcher("/api/**").authorizeRequests().anyRequest().hasRole("USER").and().httpBasic().and().csrf()
	// .disable();
	// }
	// }
	//
	// @Configuration
	// @Order(2)
	// public static class JenkinsHookSecurityConfig extends WebSecurityConfigurerAdapter {
	//
	// @Override
	// public void configure(final WebSecurity web) throws Exception {
	//
	// // debug, make it configurable
	// web.debug(false);
	// }
	//
	// @Override
	// protected void configure(final HttpSecurity http) throws Exception {
	//
	// // http.antMatcher("/api/**").authorizeRequests().antMatchers("/api/**").hasRole("USER").and().httpBasic();
	// http.antMatcher("/jenkins/**").authorizeRequests().anyRequest().hasRole("USER").and().httpBasic().and()
	// .csrf().disable();
	// }
	// }
	//
	// /**
	// * Make sure that '/**' comes last in 'config-order'
	// *
	// * @author jbellmann
	// */
	// @Configuration
	// @Order(5)
	// public static class UiSecurityConfig extends WebSecurityConfigurerAdapter {
	//
	// @Override
	// public void configure(final WebSecurity web) throws Exception {
	//
	// // resources to ignore
	// // debug, make it configurable
	// web.debug(false).ignoring().antMatchers("/css/**", "/js/**", "/asset/**", "/lib/**", "/img/**",
	// "favicon.ico");
	// }
	//
	// @Override
	// protected void configure(final HttpSecurity http) throws Exception {
	//
	// http.antMatcher("/**").authorizeRequests().antMatchers("/signup", "/about").permitAll().anyRequest()
	// .hasRole("USER").and().formLogin().loginPage("/login").permitAll() // if csrf is not disabled then
	//
	// // '/logout'-request has
	//
	// // to be a post
	// .and().csrf().disable();
	// }
	// }
}
