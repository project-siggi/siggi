package de.sigmalab.siggi.gw.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

import de.sigmalab.siggi.gw.Builds;
import de.sigmalab.siggi.gw.Project;
import de.sigmalab.siggi.gw.Projects;
import de.sigmalab.siggi.gw.worker.Worker;
import de.sigmalab.siggi.task.BuildTask;

/**
 * Behaves like Jenkins. Can be used with Stash and the Jenkins-Webhook-Plugin.
 *
 * @author jbellmann
 */
@Controller
public class JenkinsHook {

	private final Logger LOG = LoggerFactory.getLogger(JenkinsHook.class);

	private static final Splitter URL_SPLITTER = Splitter.on("/").omitEmptyStrings().trimResults();

	private final Worker worker;

	private final Projects projects;

	private final Builds builds;

	@Autowired
	public JenkinsHook(final Worker worker, Projects projects, Builds builds) {
		this.worker = worker;
		this.projects = projects;
		this.builds = builds;
	}

	@RequestMapping(value = "/jenkins/git/notifyCommit")
	@ResponseBody
	public ResponseEntity<String> hook(@RequestParam final String url,
			@RequestParam(required = false) final String branches, @RequestParam(required = false) final String sha1) {
		LOG.debug("Got url : {} , branches : {} , commitId : {}", url, branches, sha1);

		StringBuilder sb = new StringBuilder();
		sb.append("url=").append(url);
		sb.append("&");
		sb.append("branches=").append(branches);
		sb.append("&");
		sb.append("sha1=").append(sha1);

		String message = sb.toString();

		// get all projects with url
		Iterable<Project> projectsWithRepository = this.projects.findAllProjectsWithUrl(url);
		for (Project p : projectsWithRepository) {
			if (p.isActive()) {

				int buildNumber = this.builds.nextBuildnumber(p.getProjectId());

				BuildTask task = new BuildTask();
				task.setBranch(branches);
				task.setUrl(url);
				task.setSha1(sha1);
				//
				task.setBuildnumber(String.valueOf(buildNumber));
				task.setAccount(p.getAccount());
				task.setProject(p.getName());

				this.worker.doBuild(task);
			}
		}

		createProjectsFromUrl(url);

		LOG.debug("message sent: {}", message);

		//
		return new ResponseEntity<String>("Cool, we got this : {" + message + "}", HttpStatus.OK);
	}

	protected void createProjectsFromUrl(String url) {
		if (StringUtils.isEmpty(url)) {
			return;
		}
		Iterable<String> splittedUrl = URL_SPLITTER.split(url);
		int size = Iterables.size(splittedUrl);
		String repository = Iterables.get(splittedUrl, size - 1);
		String project = repository.replace(".git", "");
		String account = Iterables.get(splittedUrl, size - 2);
		List<Project> projectsForAccount = projects.findProjectsForAccount(account);

		boolean found = false;
		for (Project p : projectsForAccount) {
			if (project.equals(p.getName())) {
				found = true;
			}
		}

		if (!found) {
			Project p = new Project(account, project, url);
			p.setActive(false);
			this.projects.addProject(p);
		}

	}

}
