package de.sigmalab.siggi.gw.controller.account;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import de.sigmalab.siggi.gw.Build;
import de.sigmalab.siggi.gw.Projects;
import de.sigmalab.siggi.gw.controller.project.ReverseBuildComparator;

@Controller
public class AccountController {

    @Autowired
    private Projects projects;

    @RequestMapping(value = "/{account}")
    public String showAccount(@PathVariable final String account, final Model model) {
//
        List<Build> finishedBuilds = projects.findFinishedBuildsForAccount(account);
        Collections.sort(finishedBuilds, new ReverseBuildComparator());
        model.addAttribute("finishedBuilds", finishedBuilds);
//
        List<Build> runningBuilds = projects.findRunningBuildsForAccount(account);
        Collections.sort(runningBuilds, new ReverseBuildComparator());
        model.addAttribute("runningBuilds", runningBuilds);

        model.addAttribute("projects", projects.findProjectsForAccount(account));
        model.addAttribute("account", account);

        return "account/index";
    }

    @RequestMapping(value = "/{account}/finishedBuilds")
    public String showAccountFinishedBuilds(@PathVariable final String account, final Model model) {
//
        List<Build> finishedBuilds = projects.findFinishedBuildsForAccount(account);
        Collections.sort(finishedBuilds, new ReverseBuildComparator());
        model.addAttribute("finishedBuilds", finishedBuilds);

        return "account/index :: finishedBuilds";
    }

    @RequestMapping(value = "/{account}/runningBuilds")
    public String showAccountRunningBuilds(@PathVariable final String account, final Model model) {
//
        List<Build> runningBuilds = projects.findRunningBuildsForAccount(account);
        Collections.sort(runningBuilds, new ReverseBuildComparator());
        model.addAttribute("runningBuilds", runningBuilds);

        return "account/index :: runningBuilds";
    }
}
