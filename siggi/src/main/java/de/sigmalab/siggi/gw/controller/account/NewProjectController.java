package de.sigmalab.siggi.gw.controller.account;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.sigmalab.siggi.gw.Project;
import de.sigmalab.siggi.gw.Projects;

@Controller
public class NewProjectController {

    @Autowired
    private Projects projects;

    @RequestMapping(value = "/{account}", method = RequestMethod.GET, params = {"create"})
    public String prepareForm(@PathVariable final String account, final Model model) {
        model.addAttribute("account", account);
        return "account/createProject";
    }

    @RequestMapping(value = "/{account}/newProject", method = RequestMethod.POST)
    public String createProject(@Valid final NewProjectForm form, @PathVariable final String account,
            final Model model) {

        Project project = new Project(account, form.getName());
        project.setUrl(form.getRepositoryUrl());

        projects.addProject(project);

        model.addAttribute("account", account);
        return "redirect:" + account + "/index";
    }
}
