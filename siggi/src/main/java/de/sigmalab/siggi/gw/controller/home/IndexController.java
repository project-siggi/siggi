package de.sigmalab.siggi.gw.controller.home;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.common.collect.Lists;

import de.sigmalab.siggi.gw.events.AbstractActivityEvent;
import de.sigmalab.siggi.gw.events.BuildStartedActivityEvent;
import de.sigmalab.siggi.gw.events.BuildTerminatedActivityEvent;
import de.sigmalab.siggi.gw.events.ProjectActivatedActivityEvent;
import de.sigmalab.siggi.gw.events.ProjectCreatedActivityEvent;

/**
 * Have a look at sagan by pivotal for handling static pages.
 *
 * @author  jbellmann
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String home(final Model model) {
    	
		final ProjectCreatedActivityEvent projectCreatedActivityEvent = new ProjectCreatedActivityEvent();
		projectCreatedActivityEvent.setAccount("jbellmann");
		final BuildStartedActivityEvent buildStartedActivityEvent = new BuildStartedActivityEvent();
		buildStartedActivityEvent.setBranch("feature-branch/xy");
		buildStartedActivityEvent.setCommitID("e53dfw32a");
		
		final ProjectActivatedActivityEvent projectActivatedActivityEvent = new ProjectActivatedActivityEvent();
		final BuildTerminatedActivityEvent buildTerminatedActivityEvent = new BuildTerminatedActivityEvent();
		

		final List<AbstractActivityEvent> testList = Lists.newArrayList(buildTerminatedActivityEvent, buildStartedActivityEvent, projectActivatedActivityEvent, projectCreatedActivityEvent);
		
		model.addAttribute("activityList", testList);
    	
        return "pages/index";
    }

}
