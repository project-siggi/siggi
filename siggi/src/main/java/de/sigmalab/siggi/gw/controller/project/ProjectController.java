package de.sigmalab.siggi.gw.controller.project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import reactor.core.Reactor;
import de.sigmalab.siggi.gw.Build;
import de.sigmalab.siggi.gw.Id;
import de.sigmalab.siggi.gw.Project;
import de.sigmalab.siggi.gw.Projects;

@Controller
public class ProjectController {

	@Autowired private Reactor rootReactor;

	@Autowired private Projects projects;

	@RequestMapping(value = "/{account}/{project}")
	public String showProject(@PathVariable final String account, @PathVariable final String project, @RequestParam(
			value = "tab", required = false, defaultValue = "last") final String tabactive, final Model model) {

		model.addAttribute("tabactive", tabactive);
		model.addAttribute("account", account);
		model.addAttribute("project", project);

		List<Build> builds = projects.findBuildsForProject(Id.buildProjectId(account, project));
		Collections.sort(builds, new ReverseBuildComparator());
		model.addAttribute("builds", builds);

		return "project/index";
	}

	@RequestMapping(value = "/{account}/{project}/activate")
	public String activateProject(@PathVariable final String account, @PathVariable final String project) {
		List<Project> projectsForAccount = projects.findProjectsForAccount(account);
		for (Project p : projectsForAccount) {
			if (p.getName().equals(project)) {
				p.setActive(true);
			}
		}
		return "redirect:/{account}/{project}";

	}

	/**
	 * Updates made by javascript.
	 *
	 * @param account
	 * @param project
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{account}/{project}/update")
	public String showPartialProjectList(@PathVariable final String account, @PathVariable final String project,
			final Model model) {

		List<Build> builds = projects.findBuildsForProject(Id.buildProjectId(account, project));
		Collections.sort(builds, new ReverseBuildComparator());
		model.addAttribute("builds", builds);

		return "project/index :: projectBuilds";
	}

	@RequestMapping(value = "/{account}/{project}/{buildnumber}")
	public String showBuild(@PathVariable final String account, @PathVariable final String project,
			@PathVariable final String buildnumber, final Model model) {

		model.addAttribute("account", account);
		model.addAttribute("project", project);

		Build build = projects.getBuild(Id.buildBuildId(account, project, buildnumber));
		if (build != null) {

			model.addAttribute("build", build);
		}

		return "project/build";
	}

	static class BuildComparator implements Comparator<Build> {

		@Override
		public int compare(final Build build1, final Build build2) {
			return (int) (build1.getBuildnumber() - build2.getBuildnumber());
		}

	}
}
