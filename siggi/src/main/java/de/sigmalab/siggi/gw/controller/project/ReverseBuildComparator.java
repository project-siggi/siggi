package de.sigmalab.siggi.gw.controller.project;

import java.util.Comparator;

import de.sigmalab.siggi.gw.Build;

public class ReverseBuildComparator implements Comparator<Build> {

    @Override
    public int compare(final Build build1, final Build build2) {
        return (int) (build1.getBuildnumber() - build2.getBuildnumber()) * (-1);
    }
}
