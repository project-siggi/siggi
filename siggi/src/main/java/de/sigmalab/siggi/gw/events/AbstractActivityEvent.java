package de.sigmalab.siggi.gw.events;

import java.util.Date;

public abstract class AbstractActivityEvent {
	
	private final String type;
	
	private final Date created = new Date();

	public AbstractActivityEvent(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public Date getCreated() {
		return created;
	}
	
	

}
