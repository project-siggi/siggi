package de.sigmalab.siggi.gw.events;

import de.sigmalab.siggi.task.BuildTask;

import reactor.event.Event;

/**
 * @author  jbellmann
 */
public class BuildEvent extends Event<BuildTask> {

    private static final long serialVersionUID = 1L;

    public BuildEvent(final BuildTask buildTask) {
        super(buildTask);
    }
}
