package de.sigmalab.siggi.gw.events;

public class BuildStartedActivityEvent extends AbstractActivityEvent {
	
	private String branch;
	
	private String commitID;

	public BuildStartedActivityEvent() {
		super("build-started-activity");
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getCommitID() {
		return commitID;
	}

	public void setCommitID(String commitID) {
		this.commitID = commitID;
	}
	
	

}
