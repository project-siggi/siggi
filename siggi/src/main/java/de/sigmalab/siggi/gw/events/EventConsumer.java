package de.sigmalab.siggi.gw.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import de.sigmalab.siggi.gw.Build;
import de.sigmalab.siggi.gw.Projects;
import de.sigmalab.siggi.task.BuildTask;

import reactor.spring.context.annotation.Consumer;
import reactor.spring.context.annotation.Selector;

/**
 * @author  jbellmann
 */
@Consumer
public class EventConsumer {

    private final Logger logger = LoggerFactory.getLogger(EventConsumer.class);

    private final Projects projects;

    @Autowired
    public EventConsumer(final Projects projects) {
        this.projects = projects;
    }

    @Selector(value = "build.started", reactor = "@rootReactor")
    public void handleEvent(final BuildEvent buildStartedEvent) {
        BuildTask buildTask = buildStartedEvent.getData();
        Build b = this.projects.getBuild(buildTask.getBuildId());
        if (b != null) {

            b.setState("RUNNING");
        }

        // save not needed in memory
        logger.info("Build started {}", buildTask);
    }

    @Selector(value = "build.success", reactor = "@rootReactor")
    public void handleSuccessEvent(final BuildEvent buildStartedEvent) {
        BuildTask buildTask = buildStartedEvent.getData();
        Build b = this.projects.getBuild(buildTask.getBuildId());
        if (b != null) {

            b.setState("SUCCESS");
        }

        // save not needed in memory
        logger.info("Build started {}", buildTask);
    }

    @Selector(value = "build.failed", reactor = "@rootReactor")
    public void handleFailedEvent(final BuildEvent buildStartedEvent) {
        BuildTask buildTask = buildStartedEvent.getData();
        Build b = this.projects.getBuild(buildTask.getBuildId());
        if (b != null) {

            b.setState("FAILED");
        }

        // save not needed in memory
        logger.info("Build started {}", buildTask);
    }

    @Selector(value = "build.enqueued", reactor = "@rootReactor")
    public void handleBuildEnqueuedEvent(final BuildEvent buildEnqueuedEvent) {
        BuildTask buildTask = buildEnqueuedEvent.getData();
        Build b = this.projects.getBuild(buildTask.getBuildId());
        if (b != null) {

            b.setState("ENQUEUED");
        } else {
            Build build = new Build(buildTask.getProjectId(), Long.valueOf(buildTask.getBuildnumber()));
            build.setState("ENQUEUED");
            this.projects.addBuild(build);
            logger.info("Build added {}", buildTask);
        }

        // save not needed in memory
        logger.info("Build enqueued {}", buildTask);
    }

}
