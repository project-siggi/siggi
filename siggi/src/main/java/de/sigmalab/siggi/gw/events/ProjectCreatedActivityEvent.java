package de.sigmalab.siggi.gw.events;

import de.sigmalab.siggi.gw.Project;

public class ProjectCreatedActivityEvent extends AbstractActivityEvent {
	
	private Project project;
	
	private String account;

	public ProjectCreatedActivityEvent() {
		super("project-created-activity");
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	
	

}
