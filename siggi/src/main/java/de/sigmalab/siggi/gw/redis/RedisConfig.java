package de.sigmalab.siggi.gw.redis;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

/**
 * Configuration for Redis.
 *
 * @author  jbellmann
 */
@Configuration
public class RedisConfig {

    @Autowired
    private RedisSettings redisSettings;

    @Bean
    public JedisConnectionFactory connectionFactory() {
        JedisConnectionFactory cf = new JedisConnectionFactory();

        cf.setHostName(redisSettings.getHost());
        cf.setPort(redisSettings.getPort());
        cf.setDatabase(redisSettings.getDbindex());
        return cf;
    }

}
