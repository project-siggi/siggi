package de.sigmalab.siggi.gw.redis;

import org.springframework.boot.context.properties.ConfigurationProperties;

import org.springframework.stereotype.Component;

/**
 * Settings for Redis-Connection.
 *
 * @author  jbellmann
 */
@Component
@ConfigurationProperties(prefix = "redis")
public class RedisSettings {

    private String host = "localhost";

    private int port = 6379;

    private int dbindex = 0;

    public String getHost() {
        return host;
    }

    public void setHost(final String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(final int port) {
        this.port = port;
    }

    public int getDbindex() {
        return dbindex;
    }

    public void setDbindex(final int dbindex) {
        this.dbindex = dbindex;
    }
}
