package de.sigmalab.siggi.gw.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.concurrent.FailureCallback;

import de.sigmalab.siggi.task.BuildTask;

/**
 * 
 * @author jbellmann
 *
 */
public final class BuildFailureCallback  implements FailureCallback{
	
	private static final Logger logger = LoggerFactory.getLogger(BuildFailureCallback.class);
	
	private final BuildTask buildTask;
	
	public BuildFailureCallback(BuildTask buildTask) {
		Assert.notNull(buildTask, "'BuildTask' should never be null");
		this.buildTask = buildTask;
	}

	@Override
	public void onFailure(Throwable ex) {
		logger.error("BUILD FAILED for Build-Task {}", buildTask, ex);
	}

}
