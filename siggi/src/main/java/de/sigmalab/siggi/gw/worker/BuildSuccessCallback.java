package de.sigmalab.siggi.gw.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.concurrent.SuccessCallback;

import de.sigmalab.siggi.task.BuildTask;

/**
 * 
 * @author jbellmann
 *
 */
public class BuildSuccessCallback implements SuccessCallback<BuildTask>{
	
	private static final Logger logger = LoggerFactory.getLogger(BuildSuccessCallback.class);

	@Override
	public void onSuccess(BuildTask result) {
		logger.info("Successfull build for {}", result);
	}

}
