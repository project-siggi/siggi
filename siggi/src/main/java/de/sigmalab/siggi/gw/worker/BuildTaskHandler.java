package de.sigmalab.siggi.gw.worker;

import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import reactor.core.Reactor;

import com.xebialabs.overthere.CmdLine;
import com.xebialabs.overthere.ConnectionOptions;
import com.xebialabs.overthere.OperatingSystemFamily;
import com.xebialabs.overthere.Overthere;
import com.xebialabs.overthere.OverthereConnection;
import com.xebialabs.overthere.OverthereFile;
import com.xebialabs.overthere.ssh.SshConnectionBuilder;
import com.xebialabs.overthere.ssh.SshConnectionType;
import com.xebialabs.overthere.util.LoggingOverthereExecutionOutputHandler;

import de.sigmalab.siggi.gw.events.BuildEvent;
import de.sigmalab.siggi.gw.worker.config.BuildConfiguration;
import de.sigmalab.siggi.gw.worker.config.BuildConfigurationBuilder;
import de.sigmalab.siggi.gw.worker.flow.Context;
import de.sigmalab.siggi.gw.worker.flow.SimpleContext;
import de.sigmalab.siggi.gw.worker.flow.Step;
import de.sigmalab.siggi.gw.worker.flow.common.StandardBuildSequence;
import de.sigmalab.siggi.gw.worker.git.YamlFileResolver;
import de.sigmalab.siggi.slaves.ConnectionDetails;
import de.sigmalab.siggi.slaves.Slave;
import de.sigmalab.siggi.slaves.SlaveManager;
import de.sigmalab.siggi.task.BuildTask;
import de.sigmalab.siggi.templating.StringTemplate;
import de.sigmalab.siggi.templating.StringTemplateLoader;
import de.sigmalab.siggi.templating.velocity.VelocityTemplateLoader;
import de.sigmalab.siggi.util.Throttle;

/**
 * @author jbellmann
 */
@Component
@Scope(SCOPE_PROTOTYPE)
public class BuildTaskHandler implements Callable<BuildTask> {

	private final Logger logger = LoggerFactory.getLogger(BuildTaskHandler.class);

	private static final Logger console = LoggerFactory.getLogger("test.console");

	private static final String START_SERVICES_SCRIPT = "/start-services";
	private static final String STOP_SERVICES_SCRIPT = "/stop-services";
	private static final String RUN_SCRIPTS_TEMPLATE = "/run";

	private BuildTask buildTask;
	private final SlaveManager slaveManager;
	private final Reactor reactor;
	private final Environment environment;

	private static final YamlFileResolver yamlFileResolver = new YamlFileResolver();
	private static final StringTemplateLoader stringTemplateLoader = new VelocityTemplateLoader();
	private static final StringTemplateContextBuilder stringTemplateContextBuilder = new StringTemplateContextBuilder();

	@Autowired
	public BuildTaskHandler(final SlaveManager slaveManager, final Reactor reactor, final Environment environment) {
		this.slaveManager = slaveManager;
		this.reactor = reactor;
		this.environment = environment;
	}

	@Override
	public BuildTask call() throws Exception {
		SimpleContext simpleContext = createContext();
		this.reactor.notify("build.started", new BuildEvent(buildTask));

		// TODO, created tempDirectory has to be deleted
		String yamlFileContent = yamlFileResolver.getYamlFileContent(buildTask);
		logger.info("YAML_FILE : \n{}", yamlFileContent);

		Map<String, Object> loadedYamlFile = new YamlFileLoader().load(yamlFileContent);
		logger.info("LOADED_YAML_FILE: \n{}", loadedYamlFile.toString());

		BuildConfiguration buildConfiguration = BuildConfigurationBuilder.createBuilder(loadedYamlFile);
		StringTemplate stringTemplate = stringTemplateLoader.getTemplate(RUN_SCRIPTS_TEMPLATE);

		String runnableScript = stringTemplate.render(stringTemplateContextBuilder
				.getContext(buildTask, buildConfiguration));

		logger.info("Runnable Script: \n{}", runnableScript);

		String slaveHint = buildConfiguration.getImage();

		// get a slave and execute build on it
		Slave slave = this.slaveManager.requestSlave(slaveHint);
		int exitCode = 0;
		OverthereConnection connection = null;

		Step buildSequence = new StandardBuildSequence();

		try {
			// buildSequence.execute(simpleContext);

			connection = buildConnection(slave);

			OverthereFile motd = connection.getFile("/home/siggi/run.sh");
			BufferedOutputStream bos = null;
			try {

				bos = new BufferedOutputStream(motd.getOutputStream());
				IOUtils.copy(new ByteArrayInputStream(runnableScript.getBytes()), bos);
			} catch (IOException e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				closeQuietly(bos);
			}

			Throttle.SECONDS(3);

			exitCode = connection.execute(LoggingOverthereExecutionOutputHandler.loggingOutputHandler(console),
					LoggingOverthereExecutionOutputHandler.loggingErrorHandler(console),
					CmdLine.build("chmod", "755", "/home/siggi/run.sh"));

			Throttle.SECONDS(3);

			exitCode = connection.execute(LoggingOverthereExecutionOutputHandler.loggingOutputHandler(console),
					LoggingOverthereExecutionOutputHandler.loggingErrorHandler(console), CmdLine.build("/home/siggi/run.sh"));

			// CLEAN UP NEEDED ON STATIC INSTANCES
			exitCode = connection.execute(LoggingOverthereExecutionOutputHandler.loggingOutputHandler(console),
					LoggingOverthereExecutionOutputHandler.loggingErrorHandler(console),
					CmdLine.build("rm", "/home/siggi/run.sh"));

			exitCode = connection.execute(LoggingOverthereExecutionOutputHandler.loggingOutputHandler(console),
					LoggingOverthereExecutionOutputHandler.loggingErrorHandler(console),
					CmdLine.build("rm", "-rf", "/home/siggi/workspace"));
			logger.info("exitCode : {}", exitCode);

			if (exitCode == 0) {
				this.reactor.notify("build.success", new BuildEvent(buildTask));
			} else {
				this.reactor.notify("build.failed", new BuildEvent(buildTask));
			}

			System.err.println("Exit code: " + exitCode);
		} finally {

			// important
			if (connection != null) {

				connection.close();
			}

			// important
			this.slaveManager.releaseSlave(slave.getId());
		}

		return this.buildTask;
	}

	protected SimpleContext createContext() {
		SimpleContext context = new SimpleContext(this.environment);

		//
		context.put("account", this.buildTask.getAccount());
		context.put("project", this.buildTask.getProject());
		context.put("buildnumber", this.buildTask.getBuildnumber());

		return context;
	}

	private Context buildContext(final Slave slave) {

		// TODO Auto-generated method stub
		return null;
	}

	protected OverthereConnection buildConnection(final Slave slave) {
		if (slave.getConnectionDetails() != null) {
			ConnectionDetails conDetails = slave.getConnectionDetails();
			ConnectionOptions options = new ConnectionOptions();
			options.set(ConnectionOptions.ADDRESS, conDetails.getHostname());
			options.set(ConnectionOptions.USERNAME, conDetails.getUsername());
			options.set(ConnectionOptions.PASSWORD, conDetails.getPassword());
			options.set(ConnectionOptions.PORT, conDetails.getPort());
			options.set(ConnectionOptions.OPERATING_SYSTEM, OperatingSystemFamily.UNIX);
			options.set(SshConnectionBuilder.CONNECTION_TYPE, SshConnectionType.SFTP);

			OverthereConnection connection = Overthere.getConnection("ssh", options);
			return connection;
		}

		throw new RuntimeException("No ConnectionDetails provided by Slave with id : " + slave.getId());
	}

	public void setBuildTask(final BuildTask buildTask) {
		Assert.notNull(buildTask);
		this.buildTask = buildTask;

	}

}
