package de.sigmalab.siggi.gw.worker;

import com.google.common.base.Function;

/**
 * Escapes commands for creating script.
 * 
 * @author jbellmann
 *
 */
final class EscapeScriptsFunction implements Function<String, String>{
	
	private static final String S = "\"";

	@Override
	public String apply(String input) {
		StringBuilder sb = new StringBuilder();
		sb.append(S).append(input).append(S);
		return sb.toString();
	}

}
