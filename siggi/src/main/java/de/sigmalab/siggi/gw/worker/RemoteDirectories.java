package de.sigmalab.siggi.gw.worker;

/**
 * @author  jbellmann
 */
public class RemoteDirectories {

    private static final String DEFAULT_SIGGI_HOME = "/home/siggi";
    private static final String DEFAULT_WORKSPACE_DIRECTORY = "workspace";

    private RemoteDirectories() {
        //
    }

    public static String buildSiggiHomePath() {
        return DEFAULT_SIGGI_HOME;
    }

    public static String buildBuildScriptPath(final String buildScriptName) {
        StringBuilder sb = new StringBuilder();
        sb.append(buildSiggiHomePath());
        sb.append("/");
        sb.append(buildScriptName);
        return sb.toString();
    }

    public static String buildWorkspacePath() {
        StringBuilder sb = new StringBuilder();
        sb.append(buildSiggiHomePath());
        sb.append("/");
        sb.append(DEFAULT_WORKSPACE_DIRECTORY);
        return sb.toString();
    }

}
