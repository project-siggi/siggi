package de.sigmalab.siggi.gw.worker;

import static com.google.common.base.Joiner.on;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.common.base.Function;

import de.sigmalab.siggi.gw.worker.config.BuildConfiguration;
import de.sigmalab.siggi.task.BuildTask;

/**
 * @author  jbellmann
 */
final class StringTemplateContextBuilder {

    private static final Function<String, String> escapeScriptsFuction = new EscapeScriptsFunction();

    Map<String, Object> getContext(final BuildTask buildTask, final BuildConfiguration buildConfiguration) {
        Map<String, Object> context = new HashMap<String, Object>();

        context.put("siggi_username", buildTask.getAccount());
        context.put("siggi_project", buildTask.getProject());
        context.put("siggi_project_build_number", buildTask.getBuildnumber());

        context.put("git_commit", buildTask.getSha1());
        context.put("git_url", buildTask.getUrl());
        context.put("git_branch", buildTask.getBranch());

        List<String> processedScripts = transformScripts(buildConfiguration.getScripts(), context);
        List<String> processedEnvs = transformScripts(buildConfiguration.getEnvs(), context);

        context.put("scripts", buildScriptsString(processedScripts));
        context.put("envs", buildScriptsString(processedEnvs));
        context.put("services", buildScriptsString(buildConfiguration.getServices()));
        return context;
    }

    private List<String> transformScripts(final List<String> scripts, final Map<String, Object> context) {
        List<String> transformedScripts = new LinkedList<String>();
        for (String script : scripts) {
            String transformed = script;
            for (Map.Entry<String, Object> entry : context.entrySet()) {
                String variable = keyToVariable(entry.getKey());
                transformed = transformed.replace(variable, entry.getValue().toString());
            }

            transformedScripts.add(transformed);
        }

        return transformedScripts;
    }

    protected String buildScriptsString(final List<String> scripts) {
        List<String> transformed = newArrayList(transform(scripts, escapeScriptsFuction));
        String scriptsString = on(" ").join(transformed);
        return scriptsString;
    }

    protected String keyToVariable(final String key) {
        StringBuilder sb = new StringBuilder();
        sb.append("${").append(key).append("}");
        return sb.toString();
    }

}
