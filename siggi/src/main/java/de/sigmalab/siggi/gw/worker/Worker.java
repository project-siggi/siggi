package de.sigmalab.siggi.gw.worker;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.ApplicationContext;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import org.springframework.stereotype.Component;

import org.springframework.util.concurrent.ListenableFuture;

import de.sigmalab.siggi.gw.events.BuildEvent;
import de.sigmalab.siggi.task.BuildTask;

import reactor.core.Reactor;

@Component
public class Worker {

    private ThreadPoolTaskExecutor tpte;

    private final ApplicationContext ac;
    private final Reactor reactor;

    @Autowired
    public Worker(final ApplicationContext applicationContext, final Reactor reactor) {
        this.ac = applicationContext;
        this.reactor = reactor;
    }

    @PostConstruct
    public void init() {
        tpte = new ThreadPoolTaskExecutor();
        tpte.setCorePoolSize(3);
        tpte.setMaxPoolSize(tpte.getCorePoolSize() + 2); // only in use when queue is full
        tpte.afterPropertiesSet();
    }

    public void doBuild(final BuildTask buildTask) {
        this.reactor.notify("build.enqueued", new BuildEvent(buildTask));

        BuildTaskHandler buildTaskHandler = this.ac.getBean(BuildTaskHandler.class);
        buildTaskHandler.setBuildTask(buildTask);

        ListenableFuture<BuildTask> lf = this.tpte.submitListenable(buildTaskHandler);
        lf.addCallback(new BuildSuccessCallback(), new BuildFailureCallback(buildTask));
    }

}
