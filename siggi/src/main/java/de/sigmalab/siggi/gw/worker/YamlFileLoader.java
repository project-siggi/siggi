package de.sigmalab.siggi.gw.worker;

import java.io.InputStream;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.yaml.snakeyaml.Yaml;

public class YamlFileLoader {

    public Map<String, Object> load(final InputStream inputStream) {
        Yaml yaml = new Yaml();
        Object loaded = yaml.load(inputStream);
        Map<String, Object> map = asMap(loaded);
        return map;
    }
    
    public Map<String, Object> load(final String content) {
        Yaml yaml = new Yaml();
        Object loaded = yaml.load(content);
        Map<String, Object> map = asMap(loaded);
        return map;
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> asMap(final Object object) {

        // YAML can have numbers as keys
        Map<String, Object> result = new LinkedHashMap<String, Object>();
        if (!(object instanceof Map)) {

            // A document can be a text literal
            result.put("document", object);
            return result;
        }

        Map<Object, Object> map = (Map<Object, Object>) object;
        for (Entry<Object, Object> entry : map.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof Map) {
                value = asMap(value);
            }

            Object key = entry.getKey();
            if (key instanceof CharSequence) {
                result.put(key.toString(), value);
            } else {

                // It has to be a map key in this case
                result.put("[" + key.toString() + "]", value);
            }
        }

        return result;
    }
}
