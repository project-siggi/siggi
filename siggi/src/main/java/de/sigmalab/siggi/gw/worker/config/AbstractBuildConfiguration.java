package de.sigmalab.siggi.gw.worker.config;

import java.util.List;

import org.springframework.util.Assert;

import com.google.common.collect.Lists;

/**
 * Base for Builder-implementations.
 *
 * @author  jbellmann
 */
public class AbstractBuildConfiguration implements BuildConfiguration {

    protected String type;

    protected List<String> scripts = Lists.newArrayList();

    protected List<String> services = Lists.newArrayList();

    protected List<String> envs = Lists.newArrayList();

    // same as NO_IMAGE_DESCRIPTOR, but avoid dependency yet
    protected String image = "NULL";

    public AbstractBuildConfiguration(final String type) {
        Assert.hasText(type, "'type' should not be null or empty");
        this.type = type;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public List<String> getScripts() {
        return scripts;
    }

    public void setScripts(final List<String> scripts) {
        this.scripts = scripts;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(final List<String> services) {
        this.services = services;
    }

    public List<String> getEnvs() {
        return envs;
    }

    public void setEnvs(final List<String> envs) {
        this.envs = envs;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

}
