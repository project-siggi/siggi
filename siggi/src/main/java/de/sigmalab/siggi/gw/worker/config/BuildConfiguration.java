package de.sigmalab.siggi.gw.worker.config;

import java.util.List;

/**
 * Holds configuration from 'siggi.yml'.
 *
 * @author  jbellmann
 */
public interface BuildConfiguration {

    /**
     * Identifier for builder-type. Represented by 'language' in yaml-file (e.g. 'language: java')
     *
     * @return
     */
    String getType();

    /**
     * List of commands to execute.
     *
     * @return
     */
    List<String> getScripts();

    /**
     * List of env-variables to export.
     *
     * @return
     */
    List<String> getEnvs();

    /**
     * List of services to start.
     *
     * @return
     */
    List<String> getServices();

    /**
     * Returns the identifier of 'Image'. This identifier can be used to decide what container should be started for the
     * build.
     *
     * @return
     */
    String getImage();
}
