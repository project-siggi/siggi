package de.sigmalab.siggi.gw.worker.config;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

/**
 * @author  jbellmann
 */
public final class BuildConfigurationBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(BuildConfigurationBuilder.class);
    private static final CryptoService cryptoService = new CryptoService();

    static {
        cryptoService.init();
    }

    /**
     * Builds a configuration to execute from 'siggi.yml'-data.
     *
     * @param   buildInstructions
     *
     * @return
     */
    public static BuildConfiguration createBuilder(final Map<String, Object> buildInstructions) {
        if ("java".equals(buildInstructions.get("language"))) {
            JavaBuildConfiguration buildConfiguration = new JavaBuildConfiguration();
            Object scripts = buildInstructions.get("scripts");
            if (scripts != null) {

                List<String> scriptsList = Lists.newArrayList();
                if (scripts instanceof String) {
                    scriptsList.add((String) scripts);
                } else {
                    scriptsList.addAll((List<String>) scripts);
                }

                buildConfiguration.setScripts(scriptsList);
            }

            resolveServices(buildConfiguration, buildInstructions);

            resolveEnvs(buildConfiguration, buildInstructions);

            return buildConfiguration;
        }

        throw new RuntimeException("Language not supported yet.");
    }

    protected static void resolveServices(final JavaBuildConfiguration buildConfiguration,
            final Map<String, Object> buildInstructions) {
        Object services = buildInstructions.get("services");
        if (services != null) {

            List<String> servicesList = Lists.newArrayList();
            if (services instanceof String) {
                servicesList.add((String) services);
            } else {
                servicesList.addAll((List<String>) services);
            }

            buildConfiguration.setServices(servicesList);
        }

    }

    protected static void resolveEnvs(final AbstractBuildConfiguration buildConfiguration,
            final Map<String, Object> buildInstructions) {

        // environment settings
        Map<String, Object> env = (Map<String, Object>) buildInstructions.get("env");
        if (env != null) {
            LOG.info("GOT ENV-ENTRIES : {}", env);
// System.out.println(env.getClass().getName());
// System.out.println(env);

            List<?> globalEntry = (List<?>) env.get("global");
            if (globalEntry != null) {

                // System.out.println(globalEntry.getClass().getName());
                for (Object entry : globalEntry) {
                    if (entry instanceof String) {
                        buildConfiguration.getEnvs().add((String) entry);
                        LOG.info("ADDED : {}", entry);
                    } else if (entry instanceof Map) {
                        Map<String, Object> secureEnv = (Map<String, Object>) entry;
                        String encryptedEnv = (String) secureEnv.get("secure");
                        buildConfiguration.getEnvs().add(cryptoService.decrypt(encryptedEnv));
                        LOG.info("ADDED DECRYPTED : {}", entry);
                    }
                }
            }
        }
    }
}
