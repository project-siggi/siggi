package de.sigmalab.siggi.gw.worker.config;

import javax.annotation.PostConstruct;

import org.jasypt.util.text.BasicTextEncryptor;

import org.springframework.stereotype.Service;

/**
 * Just for testing.
 *
 * @author  jbellmann
 */
@Service
public class CryptoService {

    private BasicTextEncryptor textEncryptor;

    public String encrypt(final String messageToEncrypt) {
        return this.textEncryptor.encrypt(messageToEncrypt);
    }

    public String decrypt(final String encryptedMessage) {
        return this.textEncryptor.decrypt(encryptedMessage);
    }

    @PostConstruct
    public void init() {
        textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword("geheim");
    }
}
