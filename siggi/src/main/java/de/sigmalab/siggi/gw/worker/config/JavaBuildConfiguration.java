package de.sigmalab.siggi.gw.worker.config;

import com.google.common.collect.Lists;

/**
 * @author  jbellmann
 */
public class JavaBuildConfiguration extends AbstractBuildConfiguration {

    public JavaBuildConfiguration() {
        super("java");

        // set an default script
        setScripts(Lists.newArrayList("mvn install -Dmaven.test.skip"));
    }

}
