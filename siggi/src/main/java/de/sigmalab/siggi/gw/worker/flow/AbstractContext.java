package de.sigmalab.siggi.gw.worker.flow;

import java.io.IOException;

import java.util.HashMap;
import java.util.Map;

import org.springframework.core.env.Environment;

import org.springframework.util.Assert;

import de.sigmalab.siggi.gw.worker.remote.NoOpSshExecutor;
import de.sigmalab.siggi.gw.worker.remote.SshExecutor;

/**
 * Implements some base methods.
 *
 * @author  jbellmann
 */
public abstract class AbstractContext implements Context {

    protected SshExecutor sshExecutor = new NoOpSshExecutor();

    private final Map<String, String> contextValues = new HashMap<String, String>();
    private final Environment environment;

    public AbstractContext(final Environment environment) {
        this.environment = environment;
    }

    @Override
    public void log(final String message) {
        System.out.println(message);
    }

    @Override
    public SshExecutor getExecutor() {
        if (this.sshExecutor == null) {
            throw new RuntimeException("'executor' should never be null");
        }

        return sshExecutor;
    }

    public void setExecutor(final SshExecutor executor) {
        Assert.notNull(executor, "'SshExecutor' should never be null");
        this.sshExecutor = executor;
    }

    @Override
    public String get(final String key) {
        String value = this.contextValues.get(key);
        if (value != null) {
            return value;
        } else {
            return this.environment.getProperty(key);
        }
    }

    public void put(final String key, final String value) {
        this.contextValues.put(key, value);
    }

    /**
     * Default empty implementation.
     */
    @Override
    public void close() throws IOException { }

}
