package de.sigmalab.siggi.gw.worker.flow;

/**
 * Base step implementation.
 *
 * @author  jbellmann
 */
public abstract class AbstractStep implements Step {

    private Context context;

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    /**
     * Empty implementation by default.
     *
     * @param  context
     */
    protected void beforeExecution(final Context context) { }

    /**
     * Empty implementation by default.
     *
     * @param  context
     */
    protected void afterExecution(final Context context) { }

    @Override
    public void execute(final Context context) {
        this.context = context;
        beforeExecution(context);
        doExecute(context);
        afterExecution(context);
    }

    protected void log(final String message) {
        this.context.log(message);
    }

    protected abstract void doExecute(final Context context);

    protected String getProcessId() {
        return this.context.get("ndt.deployment.processid");
    }

    protected String getDeploymentId() {
        return this.context.get("ndt.deployment.id");
    }

    protected Context getContext() {
        return this.context;
    }

// protected String getGroupId() {
// return getContext().get(Context.Keys.GROUPID, String.class);
// }
//
// protected String getArtifactId() {
// return getContext().get(Context.Keys.ARTIFACTID, String.class);
// }
//
// protected String getVersion() {
// return getContext().get(Context.Keys.VERSION, String.class);
// }
//
// protected String getStage() {
// return getContext().get(Context.Keys.STAGE, String.class);
// }

// protected String buildStagedKey(final String key) {
// StringBuilder sb = new StringBuilder();
// sb.append(getStage()).append(".").append(key);
// return sb.toString();
// }
}
