package de.sigmalab.siggi.gw.worker.flow;

import java.io.Closeable;

import de.sigmalab.siggi.gw.worker.remote.SshExecutor;

/**
 * An 'context' goes through a sequence of steps.
 *
 * @author  jbellmann
 */
public interface Context extends Closeable {

    String get(String key);

    void put(String key, String value);

    void log(String message);

    SshExecutor getExecutor();
}
