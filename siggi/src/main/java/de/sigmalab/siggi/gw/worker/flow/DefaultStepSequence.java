package de.sigmalab.siggi.gw.worker.flow;

import java.io.Closeable;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author  jbellmann
 */
public class DefaultStepSequence implements StepSequence {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultStepSequence.class);

    private final List<Step> sequence = new LinkedList<Step>();

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public void add(final Step step) {
        this.sequence.add(step);
    }

    @Override
    public void execute(final Context context) {
        context.log("Start sequence ...");
        for (Step s : sequence) {
            context.log("Next Step : " + s.getName());
            try {
                s.execute(context);
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                context.log(e.getMessage());
                break;
            }
        }

        context.log("Sequence finished");

        closeContext(context);
    }

    protected void closeContext(final Context context) {
        if (context instanceof Closeable) {
            IOUtils.closeQuietly((Closeable) context);
        }
    }

}
