package de.sigmalab.siggi.gw.worker.flow;

import static de.sigmalab.siggi.gw.worker.RemoteDirectories.buildBuildScriptPath;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.env.Environment;

/**
 * Implementation of {@link Context} to execute {@link Step}s.
 *
 * @author  jbellmann
 */
public class SimpleContext extends AbstractContext {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleContext.class);

    private Writer writer;
    private final String executionId = UUID.randomUUID().toString().replace("-", "");

    public SimpleContext(final Environment environment) {
        super(environment);
        put("executionId", executionId);
        put("siggi.buildscript.name", executionId);
        put("siggi.buildscript.remotePath", buildBuildScriptPath(get("siggi.buildscript.name")));
    }

    @Override
    public void log(final String line) {
        if (writer == null) {

            String logsDir = get("user.log.dir");
            File deploymentLogDirectory = new File(logsDir, get("buildnumber"));
            boolean created = deploymentLogDirectory.mkdirs();
            if (!created) {
                LOG.warn("Folder {} not created. Maybe it exists before?", deploymentLogDirectory.getAbsolutePath());
            }

            File deploymentLogFile = new File(deploymentLogDirectory, "sequence.log");

            try {
                created = deploymentLogFile.createNewFile();
                if (!created) {
                    LOG.warn("File {} not created. Maybe it exists before?", deploymentLogFile.getAbsolutePath());
                }

                writer = new FileWriter(deploymentLogFile, true);
            } catch (IOException e) {
                throw new RuntimeException("Could not created logfile-writer", e);
            }
        }

        try {
            writer.write(line);
            writer.write("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() throws IOException {
        super.close();
        if (writer != null) {
            writer.close();
        }
    }

}
