package de.sigmalab.siggi.gw.worker.flow;

/**
 * To define simple sequence of simple steps.
 *
 * @author  jbellmann
 */
public interface Step {

    /**
     * Name of Step.
     *
     * @return
     */
    String getName();

    /**
     * Execution of step with provided context.
     *
     * @param   context
     *
     * @throws  StepExecutionException
     */
    void execute(final Context context) throws StepExecutionException;

}
