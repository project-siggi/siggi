package de.sigmalab.siggi.gw.worker.flow;

/**
 * A simple sequence of steps to exectue.
 *
 * @author  jbellmann
 */
public interface StepSequence extends Step {

    void add(final Step step);
}
