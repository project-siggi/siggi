package de.sigmalab.siggi.gw.worker.flow.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import de.sigmalab.siggi.gw.worker.flow.AbstractStep;
import de.sigmalab.siggi.gw.worker.flow.Context;
import de.sigmalab.siggi.gw.worker.flow.StepExecutionException;
import de.sigmalab.siggi.util.Throttle;

/**
 * @author  jbellmann
 */
class CopyScriptToSlaveStep extends AbstractStep {

    @Override
    protected void doExecute(final Context context) {
        try {
            context.getExecutor().copyFileToRemote(buildAbsoluteRemoteFilePath(), getFileToCopyAsInputStream());
        } catch (IOException e) {
            throw new StepExecutionException(e.getMessage(), e);
        }

        Throttle.SECONDS(3);
    }

    private InputStream getFileToCopyAsInputStream() throws FileNotFoundException {

        return new FileInputStream(new File(getContext().get("siggi.buildscript.localTempFilePath")));
    }

    private String buildAbsoluteRemoteFilePath() {
        return getContext().get("siggi.buildscript.remotePath");
    }
}
