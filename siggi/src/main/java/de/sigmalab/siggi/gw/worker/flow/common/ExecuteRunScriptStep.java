package de.sigmalab.siggi.gw.worker.flow.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.sigmalab.siggi.gw.worker.flow.AbstractStep;
import de.sigmalab.siggi.gw.worker.flow.Context;

/**
 * @author  jbellmann
 */
class ExecuteRunScriptStep extends AbstractStep {

    private static final Logger LOG = LoggerFactory.getLogger(ExecuteRunScriptStep.class);

    @Override
    protected void doExecute(final Context context) {
        final String command = getExecuteRunBuildScriptCommand();
        context.log("Execute command : " + command);

        int exitCode = context.getExecutor().executeCommand(command);
        context.log("Exit-Code : " + exitCode);
    }

    protected String getExecuteRunBuildScriptCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append(getContext().get("siggi.buildscript.remotePath"));
        return sb.toString();
    }
}
