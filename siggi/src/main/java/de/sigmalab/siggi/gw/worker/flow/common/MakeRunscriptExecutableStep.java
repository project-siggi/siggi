package de.sigmalab.siggi.gw.worker.flow.common;

import de.sigmalab.siggi.gw.worker.flow.AbstractStep;
import de.sigmalab.siggi.gw.worker.flow.Context;

/**
 * Makes the runscript executable.
 *
 * @author  jbellmann
 */
class MakeRunscriptExecutableStep extends AbstractStep {

    @Override
    protected void doExecute(final Context context) {
        context.getExecutor().executeCommand(buildCommand());

    }

    protected String buildCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append("chmod 755 ");
        sb.append(getContext().get("siggi.buildscript.remotePath"));
        return sb.toString();
    }

}
