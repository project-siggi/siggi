package de.sigmalab.siggi.gw.worker.flow.common;

import de.sigmalab.siggi.gw.worker.flow.AbstractStep;
import de.sigmalab.siggi.gw.worker.flow.Context;

/**
 * @author  jbellmann
 */
class RemoveRunscriptOnSlaveStep extends AbstractStep {

    private static final String RM = "rm ";

    @Override
    protected void doExecute(final Context context) {
        context.getExecutor().executeCommand(buildRemoveRunscriptCommand());
    }

    protected String buildRemoveRunscriptCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append(RM);
        sb.append(getContext().get("siggi.buildscript.remotePath"));
        return sb.toString();
    }

}
