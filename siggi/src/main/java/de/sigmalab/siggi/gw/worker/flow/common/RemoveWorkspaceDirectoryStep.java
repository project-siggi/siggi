package de.sigmalab.siggi.gw.worker.flow.common;

import de.sigmalab.siggi.gw.worker.RemoteDirectories;
import de.sigmalab.siggi.gw.worker.flow.AbstractStep;
import de.sigmalab.siggi.gw.worker.flow.Context;

class RemoveWorkspaceDirectoryStep extends AbstractStep {

    @Override
    protected void doExecute(final Context context) {
        context.getExecutor().executeCommand(buildRemoveRunscriptCommand());

    }

    protected String buildRemoveRunscriptCommand() {
        StringBuilder sb = new StringBuilder();
        sb.append("rm -rf ");
        sb.append(RemoteDirectories.buildWorkspacePath());
        return sb.toString();
    }

}
