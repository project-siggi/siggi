package de.sigmalab.siggi.gw.worker.flow.common;

import de.sigmalab.siggi.gw.worker.flow.DefaultStepSequence;

/**
 * Defines the default-flow of the build-execution.
 *
 * @author  jbellmann
 */
public class StandardBuildSequence extends DefaultStepSequence {

    public StandardBuildSequence() {
        add(new CopyScriptToSlaveStep());
        add(new MakeRunscriptExecutableStep());
        add(new ExecuteRunScriptStep());
        add(new RemoveRunscriptOnSlaveStep());
        add(new RemoveWorkspaceDirectoryStep());
    }

}
