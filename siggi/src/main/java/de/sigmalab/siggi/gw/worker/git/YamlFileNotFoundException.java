package de.sigmalab.siggi.gw.worker.git;

import de.sigmalab.siggi.SiggiException;

/**
 * 
 * @author jbellmann
 *
 */
public class YamlFileNotFoundException extends SiggiException {
	
	private static final long serialVersionUID = 1L;
	private String repositoryUrl;
	private String commitId;
	
	public YamlFileNotFoundException(String repositoryUrl, String commitId){
		this.repositoryUrl = repositoryUrl;
		this.commitId = commitId;
	}

	public String getRepositoryUrl() {
		return repositoryUrl;
	}

	public String getCommitId() {
		return commitId;
	}

}
