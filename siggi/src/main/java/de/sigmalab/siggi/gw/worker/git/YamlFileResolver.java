package de.sigmalab.siggi.gw.worker.git;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.sigmalab.siggi.SiggiException;
import de.sigmalab.siggi.task.BuildTask;

/**
 * 
 * @author jbellmann
 *
 */
public class YamlFileResolver {

	private static final Logger logger = LoggerFactory.getLogger(YamlFileResolver.class);
	
	public String getYamlFileContent(BuildTask buildTask) throws SiggiException {
		
		Repository repository = null;
		RevWalk revWalk = null;
		ByteArrayOutputStream baos = null;
		
		try {
			File localPath = File.createTempFile("TestGitRepository", "");
			localPath.delete();

			// then clone
			logger.info("Cloning from {} to {}", buildTask.getUrl(), localPath);
			Git result = Git.cloneRepository()
			        .setURI(buildTask.getUrl())
			        .setDirectory(localPath)
			        .call();
			
			repository = result.getRepository();
			
			// find the HEAD
			ObjectId lastCommitId = ObjectId.fromString(buildTask.getSha1());
			//ObjectId lastCommitId = repository.resolve(Constants.HEAD);

			// a RevWalk allows to walk over commits based on some filtering that is defined
			revWalk = new RevWalk(repository);
			RevCommit commit = revWalk.parseCommit(lastCommitId);
			// and using commit's tree find the path
			RevTree tree = commit.getTree();
			logger.info("Having tree: {}", tree);

			// now try to find a specific file
			TreeWalk treeWalk = new TreeWalk(repository);
			treeWalk.addTree(tree);
			treeWalk.setRecursive(true);
			treeWalk.setFilter(PathFilter.create(".siggi.yml"));
			if (!treeWalk.next()) {
			    throw new YamlFileNotFoundException(buildTask.getUrl(), buildTask.getSha1());
			}

			ObjectId objectId = treeWalk.getObjectId(0);
			ObjectLoader loader = repository.open(objectId);

			// and then one can the loader to read the file
			baos = new ByteArrayOutputStream();
			loader.copyTo(baos);
			baos.flush();

			revWalk.dispose();
			revWalk = null;
			
			repository.close();
			repository = null;
			
			return new String(baos.toByteArray());

		} catch (InvalidRemoteException e) {
			new SiggiException(e.getMessage(), e);
		} catch (TransportException e) {
			new SiggiException(e.getMessage(), e);
		} catch (MissingObjectException e) {
			new SiggiException(e.getMessage(), e);
		} catch (IncorrectObjectTypeException e) {
			new SiggiException(e.getMessage(), e);
		} catch (CorruptObjectException e) {
			new SiggiException(e.getMessage(), e);
		} catch (IOException e) {
			new SiggiException(e.getMessage(), e);
		} catch (GitAPIException e) {
			new SiggiException(e.getMessage(), e);
		}finally{
			if(revWalk != null) {
				revWalk.dispose();
			}
			if(repository != null) {
				repository.close();
			}
			IOUtils.closeQuietly(baos);
		}
		
		return null;
	}
	
}
