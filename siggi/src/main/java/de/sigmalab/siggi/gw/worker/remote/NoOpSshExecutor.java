package de.sigmalab.siggi.gw.worker.remote;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simply to avoid {@link NullPointerException}.
 *
 * @author  jbellmann
 */
public class NoOpSshExecutor implements SshExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(NoOpSshExecutor.class);

    @Override
    public int executeCommand(final String command) {
        LOG.debug("EXECUTE COMMAND : {}", command);
        return 0;
    }

    @Override
    public void copyFileToRemote(final String absoluteFilePath, final InputStream fileToCoy) throws IOException {
        LOG.debug("COPY A INPUTSTREAM TO : {}", absoluteFilePath);
    }

    @Override
    public void release() {
        LOG.debug("NOTHING TO RELEASE");
    }

}
