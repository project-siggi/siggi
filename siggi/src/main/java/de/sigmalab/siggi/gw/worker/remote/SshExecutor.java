package de.sigmalab.siggi.gw.worker.remote;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author  jbellmann
 */
public interface SshExecutor {

    int executeCommand(String command);

    void copyFileToRemote(String absoluteFilePath, InputStream fileToCoy) throws IOException;

    void release();

}
