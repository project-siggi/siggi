package de.sigmalab.siggi.gw.worker.remote.overthere;

import static org.apache.commons.io.IOUtils.closeQuietly;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

import com.xebialabs.overthere.CmdLine;
import com.xebialabs.overthere.OverthereConnection;
import com.xebialabs.overthere.OverthereExecutionOutputHandler;
import com.xebialabs.overthere.OverthereFile;
import com.xebialabs.overthere.util.ConsoleOverthereExecutionOutputHandler;

import de.sigmalab.siggi.gw.worker.remote.SshExecutor;

/**
 * Implementation that uses Xebia-Overthere.
 *
 * @author  jbellmann
 */
public class OverthereSshExecutor implements SshExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(OverthereSshExecutor.class);

    private static final Splitter SPLITTER = Splitter.on(" ").trimResults().omitEmptyStrings();

    protected OverthereConnection remoteConnection;
    protected OverthereExecutionOutputHandler stdErrorHandler;
    protected OverthereExecutionOutputHandler stdOutputHandler;

    public OverthereSshExecutor(final OverthereConnection overthereConnection) {
        this(overthereConnection, ConsoleOverthereExecutionOutputHandler.sysoutHandler(),
            ConsoleOverthereExecutionOutputHandler.syserrHandler());
    }

    public OverthereSshExecutor(final OverthereConnection overthereConnection,
            final OverthereExecutionOutputHandler outputHandler) {
        this(overthereConnection, outputHandler, outputHandler);
    }

    public OverthereSshExecutor(final OverthereConnection overthereConnection,
            final OverthereExecutionOutputHandler sysoutHandler, final OverthereExecutionOutputHandler errorHandler) {
        this.remoteConnection = overthereConnection;
        this.stdOutputHandler = sysoutHandler;
        this.stdErrorHandler = errorHandler;
    }

    @Override
    public int executeCommand(final String command) {
        LOG.debug("RUN COMMAND : {}", command);

        final String[] commandSplitted = Iterables.toArray(SPLITTER.split(command), String.class);
        final CmdLine commandLine = CmdLine.build(commandSplitted);
        return this.remoteConnection.execute(getStdOutHandler(), getStdErrorHandler(), commandLine);
    }

    @Override
    public void copyFileToRemote(final String absoluteFilePath, final InputStream fileToCoy) throws IOException {
        LOG.debug("COPY FILE TO : {}", absoluteFilePath);

        final OverthereFile motd = this.remoteConnection.getFile(absoluteFilePath);
        motd.getParentFile().mkdirs();

        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(motd.getOutputStream());
            IOUtils.copy(fileToCoy, bos);
        } catch (IOException e) {

            // TODO, should we log here?
            throw e;
        } finally {
            closeQuietly(bos);
        }
    }

    /**
     * Builds the errorHandler.
     *
     * @return
     */
    protected OverthereExecutionOutputHandler getStdErrorHandler() {
        return this.stdErrorHandler;
    }

    /**
     * Builds the stdoutHandler.
     *
     * @return
     */
    protected OverthereExecutionOutputHandler getStdOutHandler() {
        return this.stdOutputHandler;
    }

    @Override
    public void release() {
        if (this.remoteConnection != null) {

            this.remoteConnection.close();
        }
    }

}
