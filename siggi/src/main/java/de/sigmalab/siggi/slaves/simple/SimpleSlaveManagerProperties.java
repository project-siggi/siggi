package de.sigmalab.siggi.slaves.simple;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

import de.sigmalab.siggi.slaves.ConnectionDetails;

/**
 * @author jbellmann
 */
@ConfigurationProperties(prefix = "siggi.slaves.simple")
public class SimpleSlaveManagerProperties {

	private final Splitter SPLITTER = Splitter.on(':').trimResults().omitEmptyStrings();
	private List<String> connections = new LinkedList<String>();

	public List<String> getConnections() {
		return connections;
	}

	public void setConnections(final List<String> connections) {
		this.connections = connections;
	}

	private List<ConnectionDetails> staticConnections;

	public List<ConnectionDetails> asStaticConnections() {
		if (this.staticConnections == null) {
			this.staticConnections = new LinkedList<ConnectionDetails>();
			for (String s : connections) {
				URI uri = URI.create(s);
				if (!"ssh".equals(uri.getScheme())) {
					throw new RuntimeException("URI should start with ssh://");
				}

				ConnectionDetails stC = new ConnectionDetails();
				stC.setHostname(uri.getHost());

				Iterable<String> splitted = SPLITTER.split(uri.getUserInfo());
				stC.setUsername(Iterables.getFirst(splitted, "UNKNOWN"));
				stC.setPassword(Iterables.getLast(splitted, "UNKNOWN"));
				stC.setPort(uri.getPort());
				staticConnections.add(stC);
			}
		}

		return staticConnections;
	}

}
