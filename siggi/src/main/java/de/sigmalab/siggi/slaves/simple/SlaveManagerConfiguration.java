package de.sigmalab.siggi.slaves.simple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.sigmalab.siggi.slaves.SlaveManager;
import de.sigmalab.slaves.simple.SimpleSlaveManager;

/**
 * @author jbellmann
 */
@Configuration
@EnableConfigurationProperties({ SimpleSlaveManagerProperties.class })
public class SlaveManagerConfiguration {

	@Autowired private SimpleSlaveManagerProperties staticConnectionSettings;

	@Bean
	public SlaveManager staticSlaveManager() {
		SimpleSlaveManager slaveManager = new SimpleSlaveManager(staticConnectionSettings.asStaticConnections());

		return slaveManager;
	}
}
