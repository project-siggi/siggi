package de.sigmalab.siggi.task;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;

/**
 * @author  jbellmann
 */
public class BuildTask {

    private String buildnumber = "-1";

    private String account = "siggi-test";

    private String project = "siggi-test";

    private String url;

    private String branch;

    private String sha1;

    private static Joiner joiner = Joiner.on("/").skipNulls();

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(final String branch) {
        this.branch = branch;
    }

    public String getSha1() {
        return sha1;
    }

    public void setSha1(final String sha1) {
        this.sha1 = sha1;
    }

    public String getBuildnumber() {
        return buildnumber;
    }

    public void setBuildnumber(final String buildnumber) {
        this.buildnumber = buildnumber;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(final String account) {
        this.account = account;
    }

    public String getProject() {
        return project;
    }

    public void setProject(final String project) {
        this.project = project;
    }

    public String getProjectId() {
        return joiner.join(account, project).toString();
    }

    public String getBuildId() {
        return joiner.join(account, project, buildnumber).toString();
    }

    public String toString() {
        return Objects.toStringHelper(this).add("url", url).add("branch", branch).add("sha1", sha1)
                      .add("buildnumber", buildnumber).add("account", account).add("project", project).toString();
    }
}
