package de.sigmalab.siggi.templating;

/**
 * inspired by Keith Donald, https://jira.springsource.org/browse/SPR-9054
 * 
 * @author jbellmann
 *
 */
public interface StringTemplateLoader {
    
    StringTemplate getTemplate(String location);

}
