package de.sigmalab.siggi.templating.velocity;

import java.io.StringWriter;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.util.Assert;

import de.sigmalab.siggi.templating.StringTemplate;

/**
 * 
 * @author jbellmann
 *
 */
public class VelocityStringTemplate implements StringTemplate {

    private final Template template;
    
    public VelocityStringTemplate(Template template){
        Assert.notNull(template, "The template should never be null");
        this.template = template;
    }

    @Override
    public String render(Map<String, Object> model) {
        VelocityContext velocityContext = new VelocityContext(model);
        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);
        return stringWriter.toString();
    }

}
