package de.sigmalab.siggi.templating.velocity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.sigmalab.siggi.templating.StringTemplateLoader;

@Configuration
public class VelocityTemplateConfig {

    @Bean
    public StringTemplateLoader stringTemplateLoader() {
        return new VelocityTemplateLoader();
    }
}
