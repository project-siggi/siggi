package de.sigmalab.siggi.templating.velocity;

import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.springframework.util.Assert;

import de.sigmalab.siggi.templating.StringTemplate;
import de.sigmalab.siggi.templating.StringTemplateLoader;

/**
 * A StringTemplateLoader implemented with Velocity.
 * 
 * @author jbellmann
 *
 */
public class VelocityTemplateLoader implements StringTemplateLoader{
    
    private static final String DEFAULT_ENCODING = "UTF-8";
    
    private final String encoding;
    
    public VelocityTemplateLoader(){
        this(DEFAULT_ENCODING);
    }
    
    public VelocityTemplateLoader(String encoding){
        Properties properties = new Properties();
        properties.put("resource.loader", "class");
        properties.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(properties);
        this.encoding = encoding;
    }

    @Override
    public StringTemplate getTemplate(String location) {
        Assert.hasText(location, "The location should never be null");
        Template template = Velocity.getTemplate(location + ".vm", encoding);
        return new VelocityStringTemplate(template);
    }

}
