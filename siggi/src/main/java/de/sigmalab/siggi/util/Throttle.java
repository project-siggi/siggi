package de.sigmalab.siggi.util;

import java.util.concurrent.TimeUnit;

public final class Throttle {
	
	private Throttle(){
		// hide constructor
	}
	
	public static void FIVE_SECONDS(){
		run(5, TimeUnit.SECONDS);
	}
	
	public static void TEN_SECONDS(){
		run(10, TimeUnit.SECONDS);
	}
	
	public static void SECONDS(int value){
		run(value, TimeUnit.SECONDS);
	}
	
	public static void run(int value, TimeUnit timeUnit){
		try {
			timeUnit.sleep(value);
		} catch (InterruptedException e) {
			//ignore
		}
	}

}
