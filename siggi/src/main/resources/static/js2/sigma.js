Sigma = {

    getJson : function(params){

        defaultOpts = {
            url : '',
            method : "POST",
            accepts  : "application/json",
            contentType  : "application/x-www-form-urlencoded",
            callback : null,
            cache :false,
            dataType : "json"
        };

        var opts = $.extend(defaultOpts, params);

        console.log(opts);
        $.ajax({
            url: opts.url,
            data: opts.data,
            accepts : opts.accepts,
            type : opts.method,
            contentType : opts.contentType,
            success: function(data){
                opts.callback(data);
            },
            error :function(jqXHR, textStatus, errorThrown){
                alert(textStatus);
            },
            headers : {Accept  : "application/json"}

        });
    },

    removeSlashAtEnd : function(newPath){
        // remove slash at end
        var result = newPath;
        if (newPath.lastIndexOf("/") == newPath.length-1){
            result = newPath.substr(0, newPath.length-1)
        }
        return result;
    }
}


Sigma.accountStartView = {

    submitCreateRepositotyForm : function(url, name1, open1){
        url1 = url;
        data1 = {name : name1, open : open1};


        Sigma.getJson({
            url : url1,
            data : data1,
            method : "POST",
            callback : function(data){
                window.location.reload();
            }
        });
    }

}




Sigma.accessControlView = {
	submitCreateMembershipForm : function(url, username1, projectAccess1, projectName1, repositoryOwner1) {
		url1 = url;
		data1 = {username : username1, projectAccess : projectAccess1, projectName : projectName1, repositoryOwner : repositoryOwner1};
		
		Sigma.getJson({
			url : url1,
			data : data1,
			method : "POST",
			callback : function(data){
				window.location.reload();
			}
		});
	},

    getUserList : function(requestObject, responseCallback){
        console.log($('#usernameAuto').attr('data-url'));
        Sigma.getJson({
            url : $('#usernameAuto').attr('data-url'),
            method : "GET",
            data : {query : $('#usernameAuto').val()},
            callback : function(data){
                responseCallback(data);
            }
        });

    }
}

Sigma.accountSettingsView = {
	submitChangePasswordForm : function(url, currentPassword1, newPassword1, newPasswordAgain1) {
		url1 = url;
		data1 = {currentPassword : currentPassword1, newPassword : newPassword1, newPasswordAgain : newPasswordAgain1};
		
		Sigma.getJson({
			url : url1,
			data : data1,
			method : "POST",
			callback : function(data){
				window.location.reload();
			}
		});
	}
}

Sigma.projectView = {

    animSpeed : 500,
    currentPath  : '',
    target : 'repopathView',
    // TODO: Add templates to html page as script elements
    directoryWrapperHtml : '<div class="row-fluid animating outOfRepoview" id="rvId"><table class="table table-condensed"><thead><tr><th></th><th>Name</th><th>Age</th><th>Message</th></tr></thead><tbody class="directoryViewBody"></tbody></div>',
    directoryItemHtml : '<tr><td>{{if directory}}<i class="icon-folder-close itemIcon"></i>{{/if}}{{if !directory}}<i class="icon-file itemIcon"></i>{{/if}}</td><td><a class="repoFileLink" href="' + $('#currentRepoPath').val() +'${fullpath}" >${name}</a></td><td>${relativeCommitTime}</td><td>${shortMessage}</td></tr>',
    fileWrapperHtml : '<div class="row-fluid animating outOfRepoview" id="${$item.rvId}"><pre class="fileViewBody ${css}">${content}</pre></div>',

    loadPath : function(url, direction){
        var repoPath = ($('#currentRepoPath').val());
        var newPath = (url.replace(repoPath, ""));

        if (Sigma.projectView.currentPath != newPath){
            Sigma.getJson({
                url : url,
                method : "GET",
                callback : function(data){
                    //console.log(data);
                    $('#repopathView .row-fluid').remove();

                    $(window).unbind('statechange');
                    History.pushState({path : url},null,url);
                    if (data.directory){
                        Sigma.projectView.viewDirectory(data.result, newPath, direction);
                    } else {
                        Sigma.projectView.viewFile(data.result, newPath, direction);
                    }
                    console.log(data);
                    if (data.readmeFileContent != null){
                        Sigma.projectView.showReadme(data.readmeFileContent);

                    } else {
                        Sigma.projectView.hideReadme();
                    }
                    Sigma.projectView.bindHandler();

                }
            });
            Sigma.projectView.currentPath = newPath;
        }
    },

    viewDirectory : function(result, currentPath, direction) {
        Sigma.projectView.updateBreadcrumb(result, currentPath);
        newId = "rv_" + Sigma.projectView.getRepoViewItemId();
        wrapperHtml = Sigma.projectView.directoryWrapperHtml.replace("rvId", newId);
        $.template( "directoryWrapperHtml", wrapperHtml );
        $.template( "directoryItemHtml", Sigma.projectView.directoryItemHtml );
        $('#repopathView').append(wrapperHtml);
        $.tmpl( "directoryItemHtml", result).appendTo($('.directoryViewBody'));
        animateValue = $('#repopathView .row').width() * direction;
//        $('#' + newId).css({'left' :  animateValue + "px"});
//        $('#' + newId).transition({ x: -animateValue + 'px' }, Sigma.projectView.animSpeed);
//        $('#repopathView').transition({height: $('#' + newId).height() +"px"}, Sigma.projectView.animSpeed);
},

viewFile : function(result, currentPath, direction) {
    newId = "rv_" + Sigma.projectView.getRepoViewItemId();
    Sigma.projectView.updateBreadcrumb(result, currentPath);
    $.template( "fileWrapperHtml", Sigma.projectView.fileWrapperHtml );
    fileHtml = $.tmpl( "fileWrapperHtml", result, {rvId : newId}).appendTo($('#repopathView'));
    prettyPrint();
    animateValue = $('#repopathView .row').width() * direction;
//        $('#' + newId).css({'left' :  animateValue + "px"});
//        $('#' + newId).transition({ x: -animateValue + 'px' }, Sigma.projectView.animSpeed);
//        $('#repopathView').transition({height: $('#' + newId).height() +"px"}, Sigma.projectView.animSpeed);

},


updateBreadcrumb : function(result, currentPath){
    $('#repopathBreadcrumb').empty();
    $('#repopathBreadcrumb').append('<li><a class="breadcrumbLink" href="'+ $('#currentRepoPath').val() +'">[ ' + $('#currentProjectName').val() + ' ] </a></li>');
    pathElements =  Sigma.removeSlashAtEnd(currentPath).split("/");
    var fullPath = "";
    if(currentPath.length > 0){
        $.each(pathElements, function(index, value){
            fullPath = fullPath + value + "/";
            $('#repopathBreadcrumb').append('<li><span class="divider">/ </span></li>');
            if (index < pathElements.length -1){
                $('#repopathBreadcrumb').append('<li><a class="breadcrumbLink" href="'+ $('#currentRepoPath').val() + fullPath +'">' + value + '</a></li>');
            } else {
                $('#repopathBreadcrumb').append('<li class="active">' + value + '</li>')
            }

        });
    }


},

showReadme : function(content){
    setTimeout(function(){
        $('#readmeView').empty();
        $('#readmeView').show(0);

            //$('#readmeView').html(content);
            var converter = new Showdown.converter();
            $('#readmeView').html(converter.makeHtml(content));

        }, Sigma.projectView.animSpeed);
},

hideReadme : function(){
    $('#readmeView').empty();
    $('#readmeView').hide(0);
},

bindHandler : function(){
        // links
        $('.repoFileLink').unbind('click');
        $('.breadcrumbLink').unbind('click');
        $('.repoFileLink').bind('click', function(e){
            e.preventDefault();
            // set right anim direction for double dot directory
            if($(e.target).text() == ".."){
                Sigma.projectView.loadPath($(e.target).attr('href'), -1);
            } else {
                Sigma.projectView.loadPath($(e.target).attr('href'), 1);
            }

        });

        $('.breadcrumbLink').bind('click', function(e){
            e.preventDefault();
            Sigma.projectView.loadPath($(e.target).attr('href'), -1);
        });

        // history change
        $(window).bind('statechange',function(){

            var state = History.getState();
            console.log("statechange:"+state.data.path);
            Sigma.projectView.loadPath(state.data.path, 'back');
        });
    },

    getRepoViewItemId : function(){
        var d = new Date();
        return d.getTime()+"_"+d.getMilliseconds();
    }


};

function styleCodeLines()
{
    var lis = $(".linenums li");

    $.each(lis, function (id, wert)
    {
          var firstLine = $(wert).find("span").html();

          if (firstLine.substr(0,1) == "+")
          {
            $(wert).addClass('add');
          }
          else if (firstLine.substr(0,1) == "-")
          {
            $(wert).addClass('remove');
          }
    });    
}

$(document).ready(function() 
{
    $("#cloneUrl").click(function() 
    {
        $("#cloneUrlMenu").toggle(function() 
        {
            $("#cloneLink").select();
        })
    });


    $("#reportErrorBtn").click(function() 
    {
        $("#reportErrorBtn").hide();
        $("#errorReportForm").show();
        
    });

});






