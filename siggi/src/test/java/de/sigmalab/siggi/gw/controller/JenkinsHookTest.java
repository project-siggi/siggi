package de.sigmalab.siggi.gw.controller;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import de.sigmalab.siggi.gw.InMemoryBuilds;
import de.sigmalab.siggi.gw.InMemoryProjects;
import de.sigmalab.siggi.gw.Project;
import de.sigmalab.siggi.gw.Projects;

public class JenkinsHookTest {

	private final String TESTURL = "https://github.com/sigmalab-projects/junit-ext.git";

	@Test
	public void createProjectsFromUrl() {
		Projects projects = new InMemoryProjects();
		JenkinsHook controller = new JenkinsHook(null, projects, new InMemoryBuilds());
		controller.createProjectsFromUrl(TESTURL);
		List<Project> projectsForAccount = projects.findProjectsForAccount("sigmalab-projects");
		Assertions.assertThat(projectsForAccount.size()).isGreaterThan(0);
	}

}
