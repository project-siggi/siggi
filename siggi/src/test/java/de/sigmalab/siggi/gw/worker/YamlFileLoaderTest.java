package de.sigmalab.siggi.gw.worker;

import java.util.List;
import java.util.Map;

import org.assertj.core.api.Assertions;

import org.junit.Test;

public class YamlFileLoaderTest {

    @Test
    public void testLoadYamlFile() {
        YamlFileLoader loader = new YamlFileLoader();
        Map<String, Object> loaded = loader.load(getClass().getResourceAsStream("/siggi-with-image.yml"));
        String imageIdentifier = (String) loaded.get("image");
        Assertions.assertThat(imageIdentifier).isEqualTo("jbellmann/siggi-slave:2.3");

        // environment settings
        Map<String, Object> env = (Map<String, Object>) loaded.get("env");
        System.out.println(env.getClass().getName());
        System.out.println(env);

        List<?> globalEntry = (List<?>) env.get("global");
        System.out.println(globalEntry.getClass().getName());
        for (Object entry : globalEntry) {
            System.out.println(entry.getClass().getName());
        }
    }
}
