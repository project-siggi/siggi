package de.sigmalab.siggi.gw.worker.config;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.sigmalab.siggi.gw.worker.YamlFileLoader;

public class BuildConfigurationTest {

    private static final Logger LOG = LoggerFactory.getLogger(BuildConfigurationTest.class);

    @Test
    public void testJavaBuildConfiguration() {
        YamlFileLoader loader = new YamlFileLoader();
        Map<String, Object> loaded = loader.load(getClass().getResourceAsStream("/siggi-with-image.yml"));

        AbstractBuildConfiguration bc = (AbstractBuildConfiguration) BuildConfigurationBuilder.createBuilder(loaded);
        List<String> envs = bc.getEnvs();
        LOG.info("ENVS : {}", envs);
    }

}
