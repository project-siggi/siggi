package de.sigmalab.siggi.gw.worker.config;

import org.assertj.core.api.Assertions;

import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CryptoServiceTest {
    private static final Logger LOG = LoggerFactory.getLogger(CryptoServiceTest.class);

    @Test
    public void testCryptoService() {
        CryptoService cs = new CryptoService();
        cs.init();

        String message = "DEPLOYER_OF_THE_MONTH=siggi";
        String encryptedMessage = cs.encrypt(message);
        LOG.info("ENCRYPTED MESSAGE : {}", encryptedMessage);

        String decryptedMessage = cs.decrypt(encryptedMessage);
        LOG.info("DECRYPTED MESSAGE : {}", decryptedMessage);

        Assertions.assertThat(decryptedMessage).isEqualTo(message);
    }

    @Test
    public void testCryptoServiceTwo() {
        CryptoService cs = new CryptoService();
        cs.init();

        String message = "MOST_SECURE_PASSWORD=klaus";
        String encryptedMessage = cs.encrypt(message);
        LOG.info("ENCRYPTED MESSAGE : {}", encryptedMessage);

        String decryptedMessage = cs.decrypt(encryptedMessage);
        LOG.info("DECRYPTED MESSAGE : {}", decryptedMessage);

        Assertions.assertThat(decryptedMessage).isEqualTo(message);
    }

}
