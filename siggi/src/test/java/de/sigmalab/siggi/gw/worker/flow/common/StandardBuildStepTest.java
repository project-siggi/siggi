package de.sigmalab.siggi.gw.worker.flow.common;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.mock.env.MockEnvironment;

import de.sigmalab.siggi.gw.worker.flow.SimpleContext;

public class StandardBuildStepTest {

    private static final Logger LOG = LoggerFactory.getLogger(StandardBuildStepTest.class);

    @Test
    public void testStandardBuildStep() throws IOException {
        File tempFile = File.createTempFile("_junit_siggi_buildscript", ".tmp");
        tempFile.deleteOnExit();

        SimpleContext context = new SimpleContext(new MockEnvironment());

        //
        context.put("account", "klaus");
        context.put("project", "simple");
        context.put("buildnumber", "67");

        context.put("user.log.dir", "siggiHome/users/klaus/simple");
        context.put("siggi.buildscript.localTempFilePath", tempFile.getAbsolutePath());

        StandardBuildSequence sbs = new StandardBuildSequence();
        sbs.execute(context);
        context.close();
        LOG.info("executed");
    }

}
