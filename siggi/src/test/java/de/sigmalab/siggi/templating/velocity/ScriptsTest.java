package de.sigmalab.siggi.templating.velocity;


import java.util.List;

import org.junit.Test;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public class ScriptsTest {
	
	@Test
	public void buildScriptsString(){
		List<String> scripts = Lists.newArrayList("mvn compile test-compile","mvn package","mvn install");
		List<String> transformed = Lists.newArrayList(Iterables.transform(scripts, new Function<String, String>() {
			private static final String S = "\"";
			@Override
			public String apply(String input) {
				StringBuilder sb = new StringBuilder();
				sb.append(S).append(input).append(S);
				return sb.toString();
			}
		}));
		String scriptsString = Joiner.on(" ").join(transformed);
		
		
		System.out.println(scriptsString);
	}

}
