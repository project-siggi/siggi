package de.sigmalab.siggi.templating.velocity;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.sigmalab.siggi.templating.StringTemplate;
import de.sigmalab.siggi.templating.StringTemplateLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=VelocityTemplateConfig.class)
public class VelocityStringTemplateTest {
	
	@Autowired
	private StringTemplateLoader templateLoader;

	@Test
	public void test(){
		StringTemplate template = templateLoader.getTemplate("/run");
		String script = template.render(getContext());
		System.out.println("GENERIERTES SCRIPT :");
		System.out.println("");
		System.out.println(script);
	}
	
	private Map<String,Object> getContext(){
		Map<String,Object> context = new HashMap<String,Object>();
		
		context.put("siggi_username", "htw-avs");
		context.put("siggi_project","project-template");
		context.put("siggi_project_build_number", "12");
		
		context.put("git_commit", "7293479234739");
		context.put("git_url","https://bitbucket.org/htw-avs/project-template.git");
		context.put("git_branch", "master");
		
		context.put("scripts", "\"mvn compile test-compile\" \"mvn package\" \"mvn install\"");
		return context;
	}
}
